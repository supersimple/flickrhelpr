<?php

$PAGE_TITLE = 'FLICKR photos'; #this is the page title that you want to use for your photo gallery
$BASEURL = 'http://mydomain.com/photos'; #this is the URL for your photo gallery - eg. 'http://mydomain.com/photos' - (no trailing slash)

//Set dimensions of the images on the homepage
$INDEXTHUMB = array();
$INDEXTHUMB['width'] = 180; //integers only, do not include 'px' at the end
$INDEXTHUMB['height'] = 150; //integers only, do not include 'px' at the end

//Set dimensions of the thumbnails on the photoset page
$INNERTHUMB = array();
$INNERTHUMB['width'] = 32; //integers only, do not include 'px' at the end
$INNERTHUMB['height'] = 32; //integers only, do not include 'px' at the end

//Set the number of columns of thumbnails to show on the photoset page
$THUMBCOLS = 5;

//Set the number of rows of thumbnails to show on the photoset page
$THUMBROWS = 5;

//set size of the feature photo - only choices are 'square', 'small', 'medium', 'large'
$FEATUREIMAGE = 'medium';


//SETUP FLICKR API AND ACCOUNT INFO
# http://www.flickr.com/services/api/ - to signup for an API key - it is easy!

$FLICKR = array();

$FLICKR['api_key'] = '12345678abcdefgh12345678abcdefgh'; #this is your key
$FLICKR['secret'] = '12345678abcdefgh'; #this is your secret (comes with your API key)
$FLICKR['username'] = 'Jimmy Greenjeans'; #this is your flickr account username. When you sign in, it says "welcome USERNAME", "Konnichi Wa USERNAME", "Bonjour, USERNAME", etc.

?>