<?php
 require_once("phpFlickr.php");
 require_once("globals.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?=$PAGE_TITLE?> - Flickr Photos</title>
	<?php
	require_once('imports.php');
	?>
	<style type="text/css">
		.setPhoto img{width:<?=$INDEXTHUMB['width']?>px; height:<?=$INDEXTHUMB['height']?>px;}
	</style>
</head>
<body>
<div id="contain">
<?

//create the new object
$fl = new phpFlickr("{$FLICKR['api_key']}"); #Pass in API Key as param

//get the usercode
$usercode = $fl->people_findByUsername("{$FLICKR['username']}");
$FLICKR['usercode'] = $usercode['id'];

//if you want to cache the query
//$fl->enableCache("db","mysql://");

//get all of the photosets
$fs = $fl->photosets_getList("{$FLICKR['usercode']}"); #Pass in User Id as param (eg. '46573030@N00')

$photos_url = $fl->urls_getUserPhotos("{$FLICKR['usercode']}"); #Pass in User ID as Param (eg. '46573030@N00')

//loop through the photosets
foreach($fs['photoset'] as $ps){
	
	//get photoset meta data
	$psi = $fl->photosets_getInfo($ps['id']);
	//get primary photo ID
	$ps_primary = $fl->photos_getInfo($psi['primary']);
	$ps_title = $psi['title'];	
	$ps_desc = $psi['description'];
	$ps_count = $psi['photos'];
	
	//determine which size image to get from flickr for the thumbnail based on the dimensions
	if($INDEXTHUMB['width'] <= 75 && $INDEXTHUMB['height'] <= 75){ $_getsize = 'square';}
	else
	if($INDEXTHUMB['width'] <= 180 && $INDEXTHUMB['height'] <= 180){ $_getsize = 'small';}
	else{ $_getsize = 'medium';}
	
	
	//print out primary with meta data
	echo "<div class=\"setPhoto\">";
	echo "<a href=\"photoset.php?id={$ps['id']}\"><img border=0 alt=\"$ps_title\" src=\"".$fl->buildPhotoURL($ps_primary, "$_getsize")."\" /></a>";
	echo "<h2>$ps_title <em>($ps_count)</em></h2>";
	echo "</div>";
	//echo "<p>$ps_desc</p>";

	
	//for looping through the photoset photos
	//$photoset = $fl->photosets_getPhotos($ps['id']);

	//foreach($photoset['photo'] as $photo){
	//echo "<a href=\"$photos_url$photo[id]\">";
	//echo "<img border=0 alt='$photo[title]' "."src=" . $fl->buildPhotoURL($photo, "square") . ">";
	//echo "</a>";
	//} 
		
	
	
	
}

?>
<a href="http://www.flickrhelpr.com"><img src="ims/small_logo.gif" alt="flickrhelpr.com" style="margin:15px 0 0;float:left; clear:both;" /></a>
</div>
</body>
</html>
