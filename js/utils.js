/****NOTES**********************************************************************
 
 This script will scroll the thumbnails pane vertically.
 
*******************************************************************************/

// These 2 vars control the speed and easing of the scrolling.
steps = 25; 
speed = 20;

//flag to tell when menu is moving
inMotion = false;

		function slide(side){
			
			//****TEST FOR MENU THAT IS STILL MOVING****************************/
			if(inMotion){return false;}
			
			//****get the amount of the div that is not visible*****************/
			
			vHei = sansPX(document.getElementById('mask').style.height);
			
			eHei = document.getElementById('thumbs').offsetHeight;
			
			//this matches the vertical margins of the thumbnails (top margin + bottom margin)
			bOffset = 3;
			
			hDiff = eHei - vHei;
			
			if(hDiff > vHei){hDiff = vHei -(-bOffset);}else{hDiff = hDiff -(-bOffset);}
			
			
			//******************************************************************/


			//****test for direction********************************************/
			
			
			if(side == 'up'){
				//if the current size is greater than our object, no need to go any further
				if(currentTop() < 0){
					//get the scroll on
					b = Math.abs(currentTop());
					t = 0;
					d = steps;
					c = hDiff;
					
					inMotion = true;
					
					scrolling = setInterval(scrollUp, speed);
				}else{
					return false;
				}
			
			}else if(side == 'down'){
				
				if(currentTop() > -(eHei - vHei) + 1){

					//get the scroll on
					b = Math.abs(currentTop());
					t = 0;
					d = steps;
					c = -hDiff;
					
					inMotion = true;
					
					scrolling = setInterval(scrollDown, speed);
				}else{
					return false;
				}
			
			}else{
				return false;
			}
			
			//******************************************************************/
			
		}
		
		function scrollUp(){
			if(t < d){
				t++;
				withEase = easeInOut(t, b, c, d);
				//scrollTo(withEase,0);
				document.getElementById('thumbs').style.top = withEase +'px';
			}else{
				clearInterval(scrolling);
				//reset position
				inMotion = false;

			}
		}
		
		function scrollDown(){
			if(t < d){
				t++;
				withEase = easeInOut(t, b, c, d);
				//scrollTo(withEase,0);
				document.getElementById('thumbs').style.top = withEase +'px';
			}else{
				clearInterval(scrolling);
				//reset position
				inMotion = false;
			}
		}
		
		/****EASING FUNCTION***************************************************/
		
		///////////// SINUSOIDAL EASING: sin(t) ///////////////
		// sinusoidal easing in - accelerating from zero velocity
		// t: current time, b: beginning value, c: change in position, d: duration
		// sinusoidal easing in/out - accelerating until halfway, then decelerating
		function easeInOut (t, b, c, d){
			return Math.ceil(-c/2 * (Math.cos(Math.PI*t/d) - 1) - (b));
		}
		
		/****GET THE CURRENT POSITION******************************************/
		
		function currentTop(){
			var tempTop = document.getElementById('thumbs').style.top;
			if(tempTop == ''){return 0;}
			else{
			var asInt = tempTop.substring(0, tempTop.length-2);
			return asInt;
			}
			
		}
		
		/****GET THE MEASURE SANS PX*******************************************/
		
		function sansPX(str){
			var tempMeasure = str;
			if(tempMeasure == ''){return 0;}
			else {
				if(tempMeasure.substring(tempMeasure.length-2, tempMeasure.length) == 'px'){
					var asInt = tempMeasure.substring(0, tempMeasure.length-2);
					return asInt;
				}else{
					return tempMeasure;
				}
			}
			
		}
		
		
		/****HIDE ELEMENTS****************************************************/
		
		function initThumbs(cnt){
			var hideIDs = new Array('prevwork', 'nextwork', 'useScroller');
			if(cnt <= imagecount){hideElems(hideIDs);}
		}
		
		function hideElems(elems){
			for(var i=0; i < elems.length; i++){
				document.getElementById(elems[i]).style.display = 'none';
			}
		}
		
/*******************************************************************************/
//  END OF SCROLLING SCRIPT
/*******************************************************************************/
