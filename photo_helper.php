<?php
#This file gets data and passes it back via ajax

require_once("phpFlickr.php");
require_once("globals.php");

$thisID = $_GET['id'];
$thisSet = $_GET['set'];

if(!empty($thisID)){
//create the new object
$fl = new phpFlickr("{$FLICKR['api_key']}"); #Pass in API Key as param
$photos_url = $fl->urls_getUserPhotos("{$FLICKR['usercode']}"); #Pass in User Id as param (eg. '46573030@N00')
$photo = $fl->photos_getInfo($thisID);
//print_r($photo);
//echo "\n\n=================\n\n";
	
	echo "<div id=\"bigPhoto\">";
	echo "<a href=\"$photos_url$photo[id]\">";
	echo "<img border=0 alt='$photo[title]' "."src=" . $fl->buildPhotoURL($photo, "<?=$FEATUREIMAGE?>") . " />";
	echo "</a>";
	echo "<h2>$photo[description]</h2>";
	echo "</div>";
	echo "<div id=\"bigPhotoMeta\"><a href=\"javascript: showMeta();\" id=\"meta_action\">Show Photo Details</a>";
	echo "<div id=\"bigPhotoMetaContainer\">";
	echo "<ul id=\"bigPhotoMetaList\"><li>Direct link to this photo:<br />$BASEURL/photoset.php?id=$thisSet&photo=$thisID</li>";
	echo "<li>Photo taken on: ".date("m/d/Y", strtotime($photo['dates']['taken']))."</li>";
	echo "<li><a href=\"$photos_url$photo[id]\">Click here</a> to see more details, or to order prints of this photo. Some options, like ordering prints,  will require you to sign in to Flickr or create a free account.</li><ul></div>";
	echo "</div>";
}else{
//do nothing
}
?>