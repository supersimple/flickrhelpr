<?php
 require_once("phpFlickr.php");
 require_once("globals.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><?= $PAGE_TITLE ?></title>
	<?php
	require_once('imports.php');
	
		$_scrollerwidth = ($INNERTHUMB['width'] + 9) * $THUMBCOLS;
		$_maskwidth = $_scrollerwidth + 20;
		$_buttonsleft = $_scrollerwidth + 3;
		
		$_offset = $THUMBROWS >= 3 ? ceil($THUMBROWS/5) : 0;
		$_scrollerheight = ($INNERTHUMB['height'] + 10) * $THUMBROWS + round(($THUMBROWS/3) *2.33) + $_offset;
		$_maskheight = $_scrollerheight;
		$_buttonstop = $_scrollerheight - 18;
	
	?>
	<script type="text/javascript"> 
	
	function showMeta(){
	var curVis = document.getElementById('bigPhotoMetaContainer').style.display;
	var newVis = (curVis == 'block') ? 'none' : 'block';
	var newOpt = (newVis == 'block') ? 'Hide' : 'Show';
	document.getElementById('bigPhotoMetaContainer').style.display = newVis;
	document.getElementById('meta_action').innerHTML = newOpt +' Photo Details';
	}

/****AJAX HELPER FUNCTIONS*****************************************************/

function showPhoto(id){

	
	var url = 'photo_helper.php';
	var set = '<?= $_GET['id'] ?>';
	var pars = 'id='+id+'&set='+set;
	var target = 'feature';
	var myAjax = new Ajax.Updater(target, url, {method: 'get', parameters:pars});

}

function getComments(id){
	
	var url = 'getcomments_helper.php';
	var pars = 'id='+id;
	var target = 'comments';
	var myAjax = new Ajax.Updater(target, url, {method: 'get', parameters:pars});

}

imagecount = <?= ($THUMBROWS * $THUMBCOLS); ?>;

/*******************************************************************************/		
			
	</script>
	
	<style type="text/css">
		#thumbs img{width:<?=$INNERTHUMB['width']?>px; height:<?=$INNERTHUMB['height']?>px;}
		
		#thumbs{width:<?=$_scrollerwidth?>px;}
		#mask{width:<?=$_maskwidth?>px; clip: rect(0px, <?=$_scrollerwidth?>px, <?=$_maskheight?>px, 0);}
		
		#thumbarea{width:<?=$_maskwidth?>px;height:<?=$_maskheight?>px;}
		
		#prevwork{position:absolute; top:3px; left:<?=$_buttonsleft?>px;}
		#nextwork{position:absolute; top:<?=$_buttonstop?>px; left:<?=$_buttonsleft?>px;}
		
		h2.thumbhint{width:<?=$_maskwidth?>px;}
		
	</style>
</head>

<body>
<div id="contain">
<?
//create the new object
$fl = new phpFlickr($FLICKR['api_key']);

//get the usercode
$usercode = $fl->people_findByUsername("{$FLICKR['username']}");
$FLICKR['usercode'] = $usercode['id'];

//if you want to cache the query
//$fl->enableCache("db","mysql://");

$photos_url = $fl->urls_getUserPhotos("{$FLICKR['usercode']}");

$currentset = $_GET['id'];

	//get photoset meta data
	$psi = $fl->photosets_getInfo($currentset);
	$ps_count = $psi['photos'];
	//get primary photo ID
	$ps_primary = $fl->photos_getInfo($psi['primary']);
	$ps_title = $psi['title'];	
	$ps_desc = $psi['description'];
	
	//print out primary with meta data
	echo "<h1 class=\"title\">$ps_title</h1>";
	//echo "<a href=\"photoset.php?id={$ps['id']}\"><img border=0 alt=\"$ps_title\" src=\"".$fl->buildPhotoURL($ps_primary, "small")."\" /></a>";
	echo "<p class=\"title\">$ps_desc</p>";

	
	
	
	
	
	//for looping through the photoset photos
	$photoset = $fl->photosets_getPhotos($currentset);
	echo "<div id=\"thumbarea\">";
	echo "<div id=\"mask\" style=\"height:".$_maskheight."px;\">";
	echo "<div id=\"thumbs\">";
	
	//determine which size image to get from flickr for the thumbnail based on the dimensions
	if($INNERTHUMB['width'] <= 75 && $INNERTHUMB['height'] <= 75){ $_getsize = 'square';}
	else
	if($INNERTHUMB['width'] <= 180 && $INNERTHUMB['height'] <= 180){ $_getsize = 'small';}
	else{ $_getsize = 'medium';}
	
	foreach($photoset['photo'] as $photo){
	//echo "<a href=\"$photos_url$photo[id]\">";
	echo "<a href=\"javascript:showPhoto($photo[id]);\" >";
	echo "<img border=0 alt='$photo[title]' "."src=" . $fl->buildPhotoURL($photo, "$_getsize") . " />";
	echo "</a>";
	} 
	
	?>
	
	
	
</div>

<a href="javascript:void slide('up');" id="prevwork"><img src="<?=$BASEURL?>/ims/scroll_up.png" alt="UP" /></a>
<a href="javascript:void slide('down');" id="nextwork"><img src="<?=$BASEURL?>/ims/scroll_down.png" alt="DOWN" /></a>

</div>
<?php
echo "<h2 class=\"thumbhint\">Click on a thumbnail to view photo.</h2>";
echo "<h2 class=\"thumbhint\">$ps_count total images in this set. <span id=\"useScroller\">Use the scroll buttons above to view more thumbnails.</span></h2>";
echo "<h2 class=\"thumbhint\"><a href=\"index.php\">Click here</a> to go back to list of photosets.</h2>";
?>
<a href="http://www.flickrhelpr.com"><img src="ims/small_logo.gif" alt="flickrhelpr.com" style="margin:15px 0 0;" /></a>
</div>

<script type="text/javascript">void initThumbs(<?=$ps_count ?>);</script>
<div id="feature">
<?php
//big photo holder
	
	//init the first photo
	$initPhoto = "<script type=\"text/javascript\">showPhoto(%s);</script>";
	$initArg = (isset($_GET['photo'])) ? $_GET['photo'] : $ps_primary['id'];
	$initCall = sprintf($initPhoto, $initArg);
	echo $initCall;
//comments holder
//	echo "<div id=\"comments\"></div>";
	//get the comments
//	echo "<script type=\"text/javascript\">getComments($ps_primary[id]);</script>";
	
?>
</div>

</div>
</body>
</html>
